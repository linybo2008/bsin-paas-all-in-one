package me.flyray.bsin.gateway.exception;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.exception.BusinessException;

@RestController
public class ErrorController {

    /**
     * 拦截器抛出的异常，token失效错误
     *
     * @param request
     */
    @RequestMapping("/bsinError")
    public void error(HttpServletRequest request) {
        String code = request.getParameter("code");
        String message = request.getParameter("message");
        throw new BusinessException(code, message);
    }

    /**
     * 拦截器抛出的异常，token失效错误
     *
     * @param request
     */
    @RequestMapping("/exthrow")
    public void rethrow(HttpServletRequest request) {
        throw new BusinessException(ResponseCode.TOKEN_ERROR);
    }

    /**
     * 拦截器抛出的异常，sign异常
     *
     * @param request
     */
    @RequestMapping("/sign")
    public void sign(HttpServletRequest request) {
        String code = request.getParameter("code");
        String message = request.getParameter("message");
        throw new BusinessException(code, message);
    }

    /**
     * 限流异常
     *
     * @param request
     */
    @RequestMapping("/limitingError")
    public void limitingError(HttpServletRequest request) {
        String code = request.getParameter("code");
        String message = request.getParameter("message");
        throw new BusinessException(code, message);
    }

}