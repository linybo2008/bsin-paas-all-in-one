package me.flyray.bsin.gateway.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import me.flyray.bsin.gateway.domain.BsinChoreographyServiceDo;

@Repository
@Mapper
public interface ChoreographyServiceMapper {

    BsinChoreographyServiceDo selectByServiceNameAndMethodName(
            @Param("serviceName") String serviceName,
            @Param("methodName") String methodName);

}
