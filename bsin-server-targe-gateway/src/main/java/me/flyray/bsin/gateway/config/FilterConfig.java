package me.flyray.bsin.gateway.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import me.flyray.bsin.gateway.interceptor.SignValidationFilter;
import me.flyray.bsin.gateway.interceptor.TokenValidationFilter;
import me.flyray.bsin.utils.BsinServiceInvokeUtil;

/**
 * @author ：bolei
 * @date ：Created in 2021/12/17 13:00
 * @description：
 * @modified By：
 */

@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean<TokenValidationFilter> registTest1() {
        //通过FilterRegistrationBean实例设置优先级可以生效
        //通过@WebFilter无效
        FilterRegistrationBean<TokenValidationFilter> bean = new FilterRegistrationBean<TokenValidationFilter>();
        //注册自定义过滤器
        bean.setFilter(new TokenValidationFilter());
        bean.setName("tokenValidationFilter");
        bean.addUrlPatterns("/gateway", "/api-gateway", "/biz-gateway");
        //优先级，最顶级
        bean.setOrder(1);
        return bean;
    }

    /*@Bean
    public FilterRegistrationBean<IpfsTokenValidationFilter> registTest3() {
        //通过FilterRegistrationBean实例设置优先级可以生效
        //通过@WebFilter无效
        FilterRegistrationBean<IpfsTokenValidationFilter> bean = new FilterRegistrationBean<IpfsTokenValidationFilter>();
        bean.setFilter(new IpfsTokenValidationFilter());//注册自定义过滤器
        bean.setName("ipfsTokenValidationFilter");
        bean.addUrlPatterns("/ipfsUpload");
        bean.setOrder(2);//优先级，最顶级
        return bean;
    }*/

    @Bean
    public FilterRegistrationBean<SignValidationFilter> registTest2() {
        //通过FilterRegistrationBean实例设置优先级可以生效
        //通过@WebFilter无效
        FilterRegistrationBean<SignValidationFilter> bean = new FilterRegistrationBean<SignValidationFilter>();
        BsinServiceInvokeUtil bsinInvokeService = new BsinServiceInvokeUtil();
        //注册自定义过滤器
        bean.setFilter(new SignValidationFilter(bsinInvokeService));
        bean.setName("signValidationFilter");
        bean.addUrlPatterns("/api-gateway");
        //优先级，越低越优先
        bean.setOrder(10);
        return bean;
    }

}
