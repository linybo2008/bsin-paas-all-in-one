package me.flyray.bsin.server;

import org.flowable.engine.DynamicBpmnService;
import org.flowable.engine.HistoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author ：bolei
 * @date ：Created in 2020/9/15 21:25
 * @description：流程会签管理
 * @modified By：
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class BsinWorkflowEngineDynamicProcessTests {

    @Autowired
    private DynamicBpmnService dynamicBpmnService;
    @Autowired
    private HistoryService historyService;

    /**
     * 会签
     */
    @Test
    public void countersign(){
        //historyService.createNativeHistoricActivityInstanceQuery().sql()
    }

    /**
     * 任意节点跳转
     *
     */
    @Test
    public void anyJust(){
        //dynamicBpmnService.removeEnableSkipExpression();
    }


}
