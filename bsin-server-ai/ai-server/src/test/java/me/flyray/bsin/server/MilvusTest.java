package me.flyray.bsin.server;

import dev.langchain4j.data.embedding.Embedding;
import dev.langchain4j.data.segment.TextSegment;
import dev.langchain4j.model.embedding.AllMiniLmL6V2EmbeddingModel;
import dev.langchain4j.model.embedding.EmbeddingModel;
import dev.langchain4j.store.embedding.EmbeddingMatch;
import dev.langchain4j.store.embedding.EmbeddingStore;
import dev.langchain4j.store.embedding.milvus.MilvusEmbeddingStore;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class MilvusTest {

  @Before
  public void before() {}

  @Test
  public void testEmbedding() {
    EmbeddingStore<TextSegment> embeddingStore =
        MilvusEmbeddingStore.builder().host("localhost").port(19530).dimension(384).build();

    EmbeddingModel embeddingModel = new AllMiniLmL6V2EmbeddingModel();

    TextSegment segment1 = TextSegment.from("I like football.");
    System.out.println("TextSegment: \n\n" + segment1);
    System.out.println("TextSegment-toString: \n\n" + segment1.toString());
    System.out.println("TextSegment-metadata: \n\n" + segment1.metadata());
    System.out.println("TextSegment-text: \n\n" + segment1.text());
    Embedding embedding1 = embeddingModel.embed(segment1).content();
    embeddingStore.add(embedding1, segment1);

    TextSegment segment2 = TextSegment.from("The weather is good today.");
    Embedding embedding2 = embeddingModel.embed(segment2).content();
    embeddingStore.add(embedding2, segment2);


    Embedding queryEmbedding = embeddingModel.embed("What is your favourite sport?").content();
    List<EmbeddingMatch<TextSegment>> relevant = embeddingStore.findRelevant(queryEmbedding, 1);
    EmbeddingMatch<TextSegment> embeddingMatch = relevant.get(0);

    System.out.println(embeddingMatch.score()); // 0.8144287765026093
    System.out.println(embeddingMatch.embedded().text()); // I like football.
  }
}
