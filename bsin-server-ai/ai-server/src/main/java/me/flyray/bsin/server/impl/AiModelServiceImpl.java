package me.flyray.bsin.server.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.context.BsinServiceContext;
import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.facade.service.AiModelService;
import me.flyray.bsin.server.domain.AiModel;
import me.flyray.bsin.server.mapper.AiModelMapper;
import me.flyray.bsin.utils.BsinPageUtil;
import me.flyray.bsin.utils.BsinSnowflake;
import me.flyray.bsin.utils.Pagination;
import me.flyray.bsin.utils.RespBodyHandler;

/**
* @author bolei
* @description 针对表【ai_tenant_ai_model】的数据库操作Service实现
* @createDate 2023-04-25 18:40:35
*/
@Service
public class AiModelServiceImpl implements AiModelService {

    @Value("${bsin.ai.aesKey}")
    private String aesKey;
    @Autowired
    private AiModelMapper aiModelMapper;

    @Override
    public Map<String, Object> add(Map<String, Object> requestMap) {
        AiModel aiModel = BsinServiceContext.getReqBodyDto(AiModel.class, requestMap);
        AiModel aiModelResult = aiModelMapper.selectByCode(aiModel.getCode());
        if(aiModelResult != null){
            throw new BusinessException("","");
        }
        String serialNo = BsinSnowflake.getId();
        aiModel.setSerialNo(serialNo);
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, aesKey.getBytes());
        aiModel.setKey(aes.encryptHex(aiModel.getKey()));
        aiModelMapper.insert(aiModel);
        return RespBodyHandler.setRespBodyDto(aiModel);
    }

    @Override
    public Map<String, Object> delete(Map<String, Object> requestMap) {
        String serialNo = (String)requestMap.get("serialNo");
        // 删除
        aiModelMapper.deleteById(serialNo);
        return RespBodyHandler.RespBodyDto();
    }

    @Override
    public Map<String, Object> edit(Map<String, Object> requestMap) {
        AiModel aiModel = BsinServiceContext.getReqBodyDto(AiModel.class, requestMap);
        AiModel aiModelResult = aiModelMapper.selectByCode(aiModel.getCode());
        if(aiModelResult == null){
            throw new BusinessException(ResponseCode.APP_CODE_EXISTS);
        }
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, aesKey.getBytes());
        aiModel.setKey(aes.encryptHex(aiModel.getKey()));
        aiModelMapper.updateById(aiModel);
        return RespBodyHandler.setRespBodyDto(aiModel);
    }

    @Override
    public Map<String, Object> detail(Map<String, Object> requestMap) {
        String serialNo = (String)requestMap.get("serialNo");
        AiModel aiModel = aiModelMapper.selectBySerialNo(serialNo);
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, aesKey.getBytes());
        aiModel.setKey(aes.decryptStr(aiModel.getKey()));
        return RespBodyHandler.setRespBodyDto(aiModel);
    }

    @Override
    public Map<String, Object> getPageList(Map<String, Object> requestMap) {
        String name = (String)requestMap.get("name");
        String code = (String)requestMap.get("code");
        String tenantId = (String)requestMap.get("tenantId");
        Pagination pagination = (Pagination)requestMap.get("pagination");
        BsinPageUtil.pageNotNull(pagination);
        PageHelper.startPage(pagination.getPageNum(),pagination.getPageSize());
        List<AiModel> aiModels = aiModelMapper.selectPageList(tenantId,name,code);
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, aesKey.getBytes());
        for (AiModel aiModel : aiModels) {
            aiModel.setKey(aes.decryptStr(aiModel.getKey()));
        }
        PageInfo<AiModel> pageInfo = new PageInfo<AiModel>(aiModels);
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
    }

}
