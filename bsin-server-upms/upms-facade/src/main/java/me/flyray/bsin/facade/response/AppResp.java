package me.flyray.bsin.facade.response;

import lombok.Data;

@Data
public class AppResp {
    public String appId;

    public String appName;

    public String type;
}
